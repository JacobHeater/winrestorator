﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinRestorator
{
    /// <summary>
    /// The event that is causing this Restore Point
    /// to be created.
    /// </summary>
    public enum RestorePointEventType
    {
        /// <summary>
        /// A change to the system is beginning.
        /// </summary>
        BeginSystemChange = 100
    }
}
