﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinRestorator
{
    /// <summary>
    /// Status codes for the WinRestorator program.
    /// </summary>
    public enum StatusCode
    {
        OK = 0,
        Error = 1,
        RestorePointNotCreated = 2,
        TimeConstraintRestriction = 3,
        RestorePointNotEnabled = 4,
        ArgumentsInvalid = 5
    }
}
