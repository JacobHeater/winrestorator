﻿using Microsoft.Win32;
using System;
using System.Globalization;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace WinRestorator
{
    /// <summary>
    /// WinRestorator Restore Point Helper class.
    /// </summary>
    public class RestorePointHelper
    {
        /// <summary>
        /// The status code that is received when everything
        /// goes smoothly.
        /// </summary>
        public const int STATUS_CODE_SUCCESS = 0;

        /// <summary>
        /// Error code indicating that System Restore is
        /// not enabled on this system.
        /// 
        /// SEE: https://docs.microsoft.com/en-us/windows/desktop/debug/system-error-codes--1000-1299-
        /// </summary>
        public const int ERROR_CODE_SERVICE_DISABLED = 1058;

        /// <summary>
        /// The name of the CreateRestorePoint method.
        /// </summary>
        private const string METHOD_NAME_CREATERESTOREPOINT = "CreateRestorePoint";

        /// <summary>
        /// Maximum number of seconds to wait to check if
        /// the restore point was created.
        /// </summary>
        private const int MAX_WAIT_SECONDS = 10;

        /// <summary>
        /// Creates a system restore point using the given parameters.
        /// </summary>
        /// <param name="restorePointName">The name of the restore point.</param>
        /// <param name="restorePointType">The type of restore point.</param>
        /// <returns></returns>
        public StatusCode CreateSystemRestorePoint(string restorePointName, RestorePointType restorePointType)
        {

            if (string.IsNullOrWhiteSpace(restorePointName))
            {
                throw new ArgumentNullException(nameof(restorePointName));
            }

            StatusCode resultCode = StatusCode.OK;
            DateTime startTime = DateTime.Now;

            if (!CanCreateNewRestorePoint(startTime))
            {
                resultCode = StatusCode.TimeConstraintRestriction;

                // Stop processing.
                return resultCode;
            }

            try
            {
                ManagementClass managementClass = null;
                try
                {
                    ManagementScope managementScope = new ManagementScope("\\root\\default");

                    managementScope.Connect();
                    managementClass = new ManagementClass("SystemRestore");
                    managementClass.Scope = managementScope;
                    managementClass.GetMethodParameters(METHOD_NAME_CREATERESTOREPOINT);

                    object[] args = new object[3]
                    {
                      restorePointName,
                      (int)restorePointType,
                      (int)RestorePointEventType.BeginSystemChange
                    };

                    int statusCode = Convert.ToInt32(managementClass.InvokeMethod(METHOD_NAME_CREATERESTOREPOINT, args), CultureInfo.CurrentCulture);

                    if (statusCode == ERROR_CODE_SERVICE_DISABLED)
                    {
                        resultCode = StatusCode.RestorePointNotEnabled;
                    }
                    else if (statusCode == STATUS_CODE_SUCCESS || statusCode == ERROR_CODE_SERVICE_DISABLED)
                    {
                        // Give some time for the restore point
                        // status to propagate. We don't wait
                        // indefinitely.

                        int waitSeconds = 0;
                        bool isRestorePointCreated = false;

                        // Check until we've reached the max
                        // number of seconds.
                        do
                        {
                            if (IsRestorePointCreated(restorePointName, startTime))
                            {
                                isRestorePointCreated = true;
                                break;
                            }

                            // Wait 1 second, and try again.
                            Thread.Sleep(1000);

                            waitSeconds++;
                        } while (waitSeconds < MAX_WAIT_SECONDS);

                        if (isRestorePointCreated)
                        {
                            resultCode = StatusCode.OK;
                        }
                        else
                        {
                            resultCode = StatusCode.Error;
                        }
                    }

                }
                catch (Exception)
                {
                    resultCode = StatusCode.Error;
                }
                finally
                {
                    managementClass?.Dispose();
                }
            }
            catch (Exception)
            {
                resultCode = StatusCode.Error;
            }

            return resultCode;
        }

        /// <summary>
        /// Checks to see if the requested system restore point
        /// was created.
        /// </summary>
        /// <param name="description">The name of the restore point.</param>
        /// <param name="starttime">The time it was requested.</param>
        /// <returns></returns>
        private bool IsRestorePointCreated(string description, DateTime starttime)
        {
            bool flag = false;
            ManagementScope scope = new ManagementScope("\\root\\default");

            scope.Connect();

            ObjectQuery query = new ObjectQuery();
            StringBuilder stringBuilder = new StringBuilder("select * from ");

            stringBuilder.Append("SystemRestore");
            stringBuilder.Append(" where  description = '");
            stringBuilder.Append(description.Replace("'", "\\'"));
            stringBuilder.Append("'");
            query.QueryString = stringBuilder.ToString();

            try
            {
                using (ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(scope, query))
                {
                    if (managementObjectSearcher.Get().Count > 0)
                    {
                        foreach (ManagementObject managementObject in managementObjectSearcher.Get())
                        {
                            using (managementObject)
                            {
                                if (ManagementDateTimeConverter.ToDateTime(managementObject.Properties["CreationTime"].Value.ToString()).AddSeconds(1.0) >= starttime)
                                {
                                    flag = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (ManagementException)
            {
                flag = true;
            }
            catch (COMException)
            {
                flag = true;
            }
            return flag;
        }

        /// <summary>
        /// Checks against the registry if the system
        /// can create a new restore point.
        /// </summary>
        /// <param name="startTime">The operation begin time.</param>
        /// <returns></returns>
        private bool CanCreateNewRestorePoint(DateTime startTime)
        {
            Version version = Environment.OSVersion.Version;
            int num = 1440;
            bool flag = true;

            if (version.Major > 6 || version.Major == 6 && version.Minor >= 2)
            {
                using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows NT\\CurrentVersion\\SystemRestore"))
                {
                    if (registryKey != null)
                    {
                        object obj = registryKey.GetValue("SystemRestorePointCreationFrequency");
                        if (obj is int)
                            num = (int)obj;
                    }
                }

                ObjectQuery objectQuery = new ObjectQuery();
                objectQuery.QueryString = "select * from SystemRestore";
                ObjectQuery query = objectQuery;
                ManagementScope scope = new ManagementScope("\\root\\default");
                scope.Connect();

                try
                {
                    DateTime dateTime1 = DateTime.MinValue;
                    using (ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(scope, query))
                    {
                        foreach (ManagementObject managementObject in managementObjectSearcher.Get())
                        {
                            using (managementObject)
                            {
                                DateTime dateTime2 = ManagementDateTimeConverter.ToDateTime(managementObject.Properties["CreationTime"].Value.ToString());
                                if (dateTime2 > dateTime1)
                                {
                                    dateTime1 = dateTime2;
                                }
                            }
                        }
                    }
                    flag = startTime.Subtract(dateTime1).TotalMinutes >= num;
                }
                catch (ManagementException)
                {
                    flag = true;
                }
                catch (COMException)
                {
                    flag = true;
                }
            }

            return flag;
        }
    }
}
