﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WinRestorator
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            if (!HasParameter(args, "--restorePointType") || !HasParameter(args, "--restorePointName"))
            {
                Environment.Exit((int)StatusCode.ArgumentsInvalid);
                return;
            }

            Dictionary<string, string> parameters = ParseArguments(args);

            string restorePointName = parameters["restorePointName"];
            string strRestorePointType = parameters["restorePointType"];

            RestorePointType restorePointType = (RestorePointType)Enum.Parse(typeof(RestorePointType), strRestorePointType);

            RestorePointHelper restorePointHelper = new RestorePointHelper();

            StatusCode status = restorePointHelper.CreateSystemRestorePoint(restorePointName, restorePointType);

            if (status == StatusCode.OK || status == StatusCode.TimeConstraintRestriction)
            {
                Environment.Exit((int)StatusCode.OK);
            }
            else
            {
                Environment.Exit((int)status);
            }
        }

        /// <summary>
        /// Parses the arguments into a collections
        /// of parameter key value pairs.
        /// </summary>
        /// <param name="args">The arguments to parse.</param>
        /// <returns></returns>
        private static Dictionary<string, string> ParseArguments(string[] args)
        {
            if (args.Length != 2)
            {
                Environment.Exit((int)StatusCode.ArgumentsInvalid);
                return default(Dictionary<string, string>);
            }

            string[] parameters = args.Where(a => a.StartsWith("--")).Select(a => a.Replace("--", "")).ToArray();

            if (parameters.Length != 2)
            {
                Environment.Exit((int)StatusCode.ArgumentsInvalid);
                return default(Dictionary<string, string>);
            }

            Dictionary<string, string> parsedParameters = new Dictionary<string, string>();

            foreach (string param in parameters)
            {
                string[] split = param.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

                if (split.Length != 2)
                {
                    Environment.Exit((int)StatusCode.ArgumentsInvalid);
                    return default(Dictionary<string, string>);
                }

                parsedParameters[split[0]] = split[1];
            }

            return parsedParameters;
        }

        private static bool HasParameter(string[] args, string name)
        {
            return !string.IsNullOrWhiteSpace(args.Where(a => a.Contains(name)).FirstOrDefault());
        }
    }
}
