﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinRestorator
{
    /// <summary>
    /// A mapping of Restore Point types to their
    /// integer values.
    /// </summary>
    public enum RestorePointType
    {
        /// <summary>
        /// An application is being installed.
        /// </summary>
        APPLICATION_INSTALL = 0,

        /// <summary>
        /// An application is being uninstalled.
        /// </summary>
        APPLICATION_UNINSTALL = 1,

        /// <summary>
        /// A device driver is being installed.
        /// </summary>
        DEVICE_DRIVER_INSTALL = 10,

        /// <summary>
        /// System settings are being modified.
        /// </summary>
        MODIFY_SETTINGS = 12,

        /// <summary>
        /// A previously created restore point is now
        /// being deleted.
        /// </summary>
        CANCELLED_OPERATION = 13,
    }
}
